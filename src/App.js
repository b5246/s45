import { Fragment, useState,useEffect } from 'react';
import { Container } from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes} from 'react-router-dom'
import AppNavbar from './components/AppNavbar';
//import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import CourseView from './components/CourseView'
import Home from './pages/Home';
import Register from './pages/Register'
import LogIn from './pages/LogIn'
import NotFound from './pages/NotFound'
import Logout from './pages/Logout'
import './App.css';
import {UserProvider} from './UserContext'

function App() {

  const [user,setUser]= useState({
    //email: localStorage.getItem('email')
    id:null,
    isAdmin:null
  })

  const unsetUser = () =>{
    localStorage.clear()
  }


  // Use to check if the user's info is properly stored during login and  localStorage info is cleared upon logout.
  useEffect(()=>{
    console.log(user)
    console.log(localStorage)
  },[user])

  return (
    <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/courses" element={<Courses/>} />
            <Route path="/login" element={<LogIn/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/courses/:courseId" element={<CourseView/>} />
            <Route path="*" element={<NotFound/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider> 
  );
}

export default App;
