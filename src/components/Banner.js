import React,{useState} from 'react'
import {Link, NavLink} from 'react-router-dom'
//import Button from 'react-bootstrap/Button'
// Bootstrap grid system components
//import Row from 'react-bootstrap/Row'
//import Col from 'react-bootstrap/Col'
import {Col,Row,Button} from 'react-bootstrap'

export default function Banner({data}){
	console.log(data);
	const {title, content, destination, label} = data;

	return (
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Link to={destination}>{label}</Link>
			</Col>
		</Row>
	)
}

