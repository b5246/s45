import {Row,Col,Card} from 'react-bootstrap'

export default function Highlights(){
	return(
		<Row className="m-3 mb-3">
			<Col xs={12} md={4}>
				<Card style={{ width: '18rem' }} className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>Learn from Home</h2>
				    </Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card style={{ width: '18rem' }} className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>Style now, Pay later</h2>
				    </Card.Title>
				    <Card.Text>
				      Faucibus ornare suspendisse sed nisi lacus. Senectus et netus et malesuada fames ac turpis egestas. Interdum velit euismod in pellentesque.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card style={{ width: '18rem' }} className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>Be Part of Our Community</h2>
				    </Card.Title>
				    <Card.Text>
				      Enim ut tellus elementum sagittis vitae et leo duis. Netus et malesuada fames ac turpis. Proin nibh nisl condimentum id venenatis a condimentum. 
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}