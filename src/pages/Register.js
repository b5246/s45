import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Register(){
	
	const { user, setUser } = useContext(UserContext)
	// State hooks to store the values of the input fields
	const [firstName,setfirstName]=useState('')
	const [lastName,setlastName]=useState('')
	const [email, setEmail] = useState('');
	const [mobile,setmobile]=useState('')
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// State hook to determine whether submit button is enabled or not
	const [isActive,setIsActive] = useState(false)


	function registerUser(e) {
        e.preventDefault()
        fetch('http://localhost:4000/users/checkEmail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email: email,})
        })
        .then(res => res.json())
        .then(data => {
            //alert(`====> data ${data}`)
            if(!data){
                {
                    fetch('http://localhost:4000/users/register', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            password: password1,
                            mobileNo: mobile
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        //alert(`====> data ${data}`)
                        Swal.fire({
                        title: "Login Successful !",
                        icon: "success",
                        text: "Welcome to Zuitt"
                    	})
                    	window.location.replace("/login")
                    })               
                }
            }else {
                Swal.fire({
                    title: "Duplicate Email Found !",
                    icon: "error",
                    text: "Please provide a different email."
            	})
            }
        })
    }

  

	useEffect(()=>{
		if((email!== '' && password1!=='' && password2 !=='' && firstName!==''&& lastName!=='' && mobile!=='') && (password1===password2)){
			setIsActive(true)

		} else{
			setIsActive(false)
		}
	}, [email,password1,password2,firstName,lastName,mobile] )

	return (
		(user.id !== null) ?
		    <Navigate to="/login" />
		:
		<Form onSubmit={(e)=>registerUser(e)}>
			<h1>Register</h1>
			<Form.Group className="mb-3" controlId="userFirstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
				  	type="text" 
				  	placeholder="Enter first name" 
				  	value={firstName}
				  	onChange={e=>setfirstName(e.target.value)}
				  	required
				 />
			</Form.Group>
			<Form.Group className="mb-3" controlId="userLastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
				  	type="text" 
				  	placeholder="Enter last name" 
				  	value={lastName}
				  	onChange={e=>setlastName(e.target.value)}
				  	required
				 />
			</Form.Group>
		  	<Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email" 
		    	value={email}
		    	onChange={e=>setEmail(e.target.value)}
		    	required
		    />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  	</Form.Group>
		  	<Form.Group className="mb-3" controlId="usermobile">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
				  	type="tel" 
				  	placeholder="01234567890" 
				  	pattern="[0-9]{11}"
				  	value={mobile}
				  	onChange={e=>setmobile(e.target.value)}
				  	required
				 />
			</Form.Group>
		  	<Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
			    type="password" 
			    placeholder="Password"
			    value={password1}
			    onChange={e=>setPassword1(e.target.value)}
			    required
		    />
		  	</Form.Group>
		  
		    <Form.Group className="mb-3" controlId="password2">
		      <Form.Label>Verify Password</Form.Label>
		      <Form.Control 
		  	    type="password" 
		  	    placeholder=" Verify Password"
		  	    value={password2}
			    onChange={e=>setPassword2(e.target.value)}
		  	    required
		      />
		    </Form.Group>

			{/* Conditionally render submit button based on isActive state */}
			{ 
				isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
				  Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit
				</Button>
			}
		  	
		</Form>
	)
}