import React from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, Navlink } from 'react-router-dom'
import Banner from '../components/Banner'

function NotFound() {
	  const data={
	  	title: "404 - Page not found",
	  	content:"The page is unavaialble",
	  	destination: "/",
	  	label:"Back home"

	  }
	  return (
	    <Banner data={data}/>
	  )
}

export default NotFound
